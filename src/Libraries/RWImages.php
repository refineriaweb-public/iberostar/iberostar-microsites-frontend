<?php

namespace RefineriaWeb\IberostarMicrositesFrontend\Libraries;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

/**
 * Created by PhpStorm.
 * User: Raul
 * Date: 11/06/2016
 * Time: 13:46
 */
class RWImages extends Image
{

    /**
     * Resize image function
     * @param string $fileName fileName
     * @param number $width width
     * @param number $height height
     * @param string $folder folder
     * @return string
     */
    public static function resize($fileName, $width, $height, $folder = 'files')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $width = ($width === null) ? 'auto' : $width;
        $height = ($height === null) ? 'auto' : $height;

        $input = "$folder/$fileName";
        $outputFolder = "$folder/thumbs/$width-$height-resize";
        $output = "$outputFolder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        // Check if image file exits
        if (!$file::exists($input)) {
            return "";
        }

        // Check if output folder exists
        if (!$file::exists($outputFolder)) {
            $file::makeDirectory($outputFolder, $mode = 0705, true, true);
            if (!$file::exists($outputFolder)) {
                return "";
            }
        }

        if ($width === 'auto') {
            try {
                Image::make(public_path($input))
                    ->resize(null, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($output);
            } catch (\Exception $e) {
                return '/'.$input;
            }

        } else {
            if ($height === 'auto') {
                try {
                    Image::make(public_path($input))
                        ->resize($width, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($output);
                } catch (\Exception $e) {
                    return '/'.$input;
                }

            } else {
                try {
                    Image::make(public_path($input))
                        ->resize($width, $height)
                        ->save($output);
                } catch (\Exception $e) {
                    return '/'.$input;
                }
            }
        }


        // Check if output file was generated correctly
        if (!$file::exists($output)) {
            return "";
        }

        return '/'.$output;
    }

    /**
     * Crop image function
     * @param string $fileName fileName
     * @param number $width width
     * @param number $height height
     * @param string $folder folder
     * @return string
     */
    public static function crop($fileName, $width, $height, $folder = 'files')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $input = "$folder/$fileName";
        $outputFolder = "$folder/thumbs/$width-$height";
        $output = "$outputFolder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        // Check if image file exits
        if (!$file::exists($input)) {
            return "";
        }

        // Check if output folder exists
        if (!$file::exists($outputFolder)) {
            $file::makeDirectory($outputFolder, $mode = 0705, true, true);
            if (!$file::exists($outputFolder)) {
                return "";
            }
        }

        try {
            Image::make(public_path($input))
                ->crop($width, $height)
                ->save($output);
        } catch (\Exception $e) {
            return '/'.$input;
        }

        // Check if output file was generated correctly
        if (!$file::exists($output)) {
            return "";
        }

        return '/'.$output;
    }

    /**
     * Fit image function
     * @param string $fileName fileName
     * @param number $width width
     * @param number $height height
     * @param string $folder folder
     * @return string
     */
    public static function fit($fileName, $width, $height, $folder = 'files')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $input = "$folder/$fileName";

        if ($height === null) {
            $height = 'auto';
        }

        $outputFolder = "$folder/thumbs/$width-$height-fit";
        $output = "$outputFolder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        // Check if image file exits
        if (!$file::exists($input)) {
            return "";
        }

        // Check if output folder exists
        if (!$file::exists($outputFolder)) {
            $file::makeDirectory($outputFolder, $mode = 0705, true, true);
            if (!$file::exists($outputFolder)) {
                return "";
            }
        }

        if ($height !== 'auto') {
            try {
                Image::make(public_path($input))
                    ->fit($width, $height, function ($constraint) {
                        $constraint->upsize();
                    })->save($output);
            } catch (\Exception $e) {
                return '/'.$input;
            }

        } else {
            try {
                Image::make(public_path($input))
                    ->fit($width, null, function ($constraint) {
                        $constraint->upsize();
                    })
                    ->save($output);
            } catch (\Exception $e) {
                return '/'.$input;
            }
        }

        // Check if output file was generated correctly
        if (!$file::exists($output)) {
            return "";
        }

        return '/'.$output;
    }

    /**
     * Widen image function
     * @param string $fileName fileName
     * @param number $width width
     * @param number $height height
     * @param string $folder folder
     * @return string
     */
    public static function widen($fileName, $width, $folder = 'files')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $input = "$folder/$fileName";

        $outputFolder = "$folder/thumbs/$width-widen";
        $output = "$outputFolder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        // Check if image file exits
        if (!$file::exists($input)) {
            return "";
        }

        // Check if output folder exists
        if (!$file::exists($outputFolder)) {
            $file::makeDirectory($outputFolder, $mode = 0705, true, true);
            if (!$file::exists($outputFolder)) {
                return "";
            }
        }

        try {
            Image::make(public_path($input))
                ->widen($width, null, function ($constraint) {
                    $constraint->upsize();
                })->save($output);
        } catch (\Exception $e) {
            return '/'.$input;
        }

        // Check if output file was generated correctly
        if (!$file::exists($output)) {
            return "";
        }

        return '/'.$output;
    }

    /**
     * Check if Supported Extension
     * @param string $filePath
     * @return bool
     */
    public static function checkIfSupportedExtension(string $filePath)
    {
        $fileExtension = File::extension($filePath);

        $supportedFiles = [
            'jpg', 'png', 'gif'
        ];

        if (!in_array($fileExtension, $supportedFiles)) {
            return false;
        }
        return true;
    }
}

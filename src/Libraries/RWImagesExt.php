<?php

namespace RefineriaWeb\IberostarMicrositesFrontend\Libraries;

use Illuminate\Support\Facades\File;

class RWImagesExt
{

    /**
     * Resize image function
     * @param string $fileName fileName
     * @param number $width width
     * @param number $height height
     * @param string $pathFile Path file
     * @param string $folder folder
     * @return string
     */
    public static function resize($fileName, $width, $height, $pathFile, $folder = 'files-cms')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $input = "$folder/$fileName";

        $outputFolder = "$folder/thumbs/$width-$height-resize";
        $output = "$outputFolder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        // @TODO Check if file exists
        if (!$file::exists($input)) {
            if (0 === strlen($pathFile)) {
                return "";
            }

            // Try to download file
            $remoteFile = @file_get_contents($pathFile .$fileName);

            if (!$remoteFile) {
                return @file_get_contents("http://via.placeholder.com/".$width."x".$height);
            }

            if (!$file::exists($folder)) {
                $file::makeDirectory($folder, $mode = 0777, true, true);
            }

            // Guardarlo en files
            $fileStoredOK = @$file::put($input, $remoteFile);

            if (!$fileStoredOK) {
                return "";
            }

            self::resize($fileName, $width, $height, "", $folder);
        }

        return RWImages::resize($fileName, $width, $height, $folder = 'files-cms');
    }

    /**
     * Widen image function
     * @param string $fileName fileName
     * @param number $width width
     * @param string $pathFile Path file
     * @param string $folder folder
     * @return string
     */
    public static function widen($fileName, $width, $pathFile, $folder = 'files-cms')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $input = "$folder/$fileName";

        $outputFolder = "$folder/thumbs/$width-widen";
        $output = "$outputFolder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        // @TODO Check if file exists
        if (!$file::exists($input)) {
            if (0 === strlen($pathFile)) {
                return "";
            }

            // Try to download file
            $remoteFile = @file_get_contents($pathFile . $fileName);

            if (!$remoteFile) {
                return @file_get_contents("http://via.placeholder.com/".$width."x".($width / 2));
            }

            if (!$file::exists($folder)) {
                $file::makeDirectory($folder, $mode = 0777, true, true);
            }

            // Guardarlo en files
            $fileStoredOK = @$file::put($input, $remoteFile);

            if (!$fileStoredOK) {
                return "";
            }

            self::widen($fileName, $width, "", $folder);
        }

        return RWImages::widen($fileName, $width, $folder = 'files-cms');
    }

    /**
     * Crop image function
     * @param string $fileName fileName
     * @param int $width width
     * @param int $height height
     * @param string $pathFile Path file
     * @param string $folder folder
     * @return string
     */
    public static function crop($fileName, $width, $height, $pathFile, $folder = 'files-cms')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $input = "$folder/$fileName";

        $outputFolder = "$folder/thumbs/$width-$height";
        $output = "$outputFolder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        // @TODO Check if file exists
        if (!$file::exists($input)) {
            if (0 === strlen($pathFile)) {
                return "";
            }

            // Try to download file
            $remoteFile = @file_get_contents($pathFile . $fileName);

            if (!$remoteFile) {
                return @file_get_contents("http://via.placeholder.com/".$width."x".$height);
            }

            if (!$file::exists($folder)) {
                $file::makeDirectory($folder, $mode = 0777, true, true);
            }

            // Guardarlo en files
            $fileStoredOK = @$file::put($input, $remoteFile);

            if (!$fileStoredOK) {
                return "";
            }

            self::crop($fileName, $width, $height, "", $folder);
        }

        return RWImages::crop($fileName, $width, $height, $folder = 'files-cms');
    }

    /**
     * Crop image function
     * @param string $fileName fileName
     * @param int $width width
     * @param int $height height
     * @param string $pathFile Path file
     * @param string $folder folder
     * @return string
     */
    public static function fit($fileName, $width, $height, $pathFile, $folder = 'files-cms')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $input = "$folder/$fileName";

        $outputFolder = "$folder/thumbs/$width-$height-fit";
        $output = "$outputFolder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        // @TODO Check if file exists
        if (!$file::exists($input)) {
            if (0 === strlen($pathFile)) {
                return "";
            }

            // Try to download file
            $remoteFile = @file_get_contents($pathFile . $fileName);

            if (!$remoteFile) {
                return @file_get_contents("http://via.placeholder.com/".$width."x".$height);
            }

            if (!$file::exists($folder)) {
                $file::makeDirectory($folder, $mode = 0777, true, true);
            }

            // Guardarlo en files
            $fileStoredOK = @$file::put($input, $remoteFile);

            if (!$fileStoredOK) {
                return "";
            }

            self::fit($fileName, $width, $height, "", $folder);
        }

        return RWImages::fit($fileName, $width, $height, $folder = 'files-cms');
    }
}

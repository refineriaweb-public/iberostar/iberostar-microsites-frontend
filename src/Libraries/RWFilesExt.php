<?php

namespace RefineriaWeb\IberostarMicrositesFrontend\Libraries;

use Illuminate\Support\Facades\File;

/**
 * Class RWFilesExt
 * @package App\Libraries
 * @property @get Get File
 */
class RWFilesExt
{

    /**
     * Get file and save in local storage
     * @param string $fileName fileName
     * @param string $pathFile Path file
     * @param string $folder folder
     * @return string
     */
    public static function get($fileName, $pathFile, $folder = 'files-cms')
    {
        $folder =  rtrim($folder, '/');
        $folder =  ltrim($folder, '/');

        $input = "$folder/$fileName";

        $output = "$folder/$fileName";
        $file = new File();

        // Check if previusly generated
        if ($file::exists($output)) {
            return '/'.$output;
        }

        if (0 === strlen($pathFile)) {
            return "";
        }

        // Try to download file
        $remoteFile = @file_get_contents($pathFile .$fileName);

        if (!$remoteFile) {
            return '#';
        }

        if (!$file::exists($folder)) {
            $file::makeDirectory($folder, $mode = 0777, true, true);
        }

        // Guardarlo en files
        $fileStoredOK = @$file::put($input, $remoteFile);

        if (!$fileStoredOK) {
            return '#';
        }

        return '/'.$output;
    }
}

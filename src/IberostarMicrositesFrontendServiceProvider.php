<?php

namespace RefineriaWeb\IberostarMicrositesFrontend;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Facades\File;

class IberostarMicrositesFrontendServiceProvider extends RouteServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected const ROUTE_FILE_NAME = 'iberostar-microsites-frontend-routes';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->app->make('RefineriaWeb\IberostarMicrositesFrontend\IberostarMicrositesFrontend');

        AliasLoader::getInstance()->alias(
            'LaravelLocalization',
            'Mcamara\LaravelLocalization\Facades\LaravelLocalization'
        );
//
        $this->publishConfig();
        $this->publishControllers();
        $this->publishRoutes();
        $this->publishView();
        $this->publihViewsVendor();
        $this->publishRoutesLangFiles();
//        $this->publishLaravelLocalization();
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'iberostar-microsites-frontend',
            'RefineriaWeb\IberostarMicrositesFrontend\IberostarMicrositesFrontend'
        );

        $this->app->register('Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */

    public function provides()
    {
        return ['iberostar-microsites-frontend'];
    }

    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapFrontendRoutes();
    }

    protected function mapFrontendRoutes()
    {
        $routeFileName = self::ROUTE_FILE_NAME . '.php';

        $file = new File();
        if ($file::exists(base_path('routes/'.$routeFileName))) {
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/'.$routeFileName));
        }
    }

    private function publishConfig()
    {
        $configPath = __DIR__.'/../config/config.php';

        $this->publishes([
            $configPath => config_path('iberostar-microsites-frontend.php'),
        ], 'config');
    }

    private function publishControllers()
    {
        $controllersTemplateFolder =  __DIR__.'/../templates/controllers/';

        $this->publishes([
            $controllersTemplateFolder.'FrontendController.stub' => app_path('Http/Controllers/FrontendController.php'),
            $controllersTemplateFolder.'HomeController.stub' => app_path('Http/Controllers/HomeController.php'),
            $controllersTemplateFolder.'ErrorController.stub' => app_path('Http/Controllers/ErrorController.php'),
        ], 'controllers');
    }

    private function publishRoutes()
    {
        $routeFileName = self::ROUTE_FILE_NAME;
        $routesTemplateFolder =  __DIR__.'/../templates/routes/';

        $this->publishes([
            $routesTemplateFolder.$routeFileName . '.stub' => base_path('routes/' . $routeFileName . '.php'),
        ], 'routes');
    }

    private function publishView()
    {
        $folderFrontend = __DIR__ . '/../templates/views/frontend/';
        $folderFrontendOutput = base_path('resources/views/');

        $this->publishes([
            $folderFrontend . 'errors/404.blade.stub' => $folderFrontendOutput . 'errors/404.blade.php',
            $folderFrontend . 'layouts/base.blade.stub' => $folderFrontendOutput . 'layouts/base.blade.php',
            $folderFrontend . 'modules/favicon.blade.stub' => $folderFrontendOutput . 'modules/favicon.blade.php',
            $folderFrontend . 'modules/footer.blade.stub' => $folderFrontendOutput . 'modules/footer.blade.php',
            $folderFrontend . 'modules/header.blade.stub' => $folderFrontendOutput . 'modules/header.blade.php',
            $folderFrontend . 'modules/metas.blade.stub' => $folderFrontendOutput . 'modules/metas.blade.php',
        ], 'views');
    }

    private function publihViewsVendor()
    {
        $folderFrontend = __DIR__ . '/../templates/views/vendor/cookieConsent/';
        $folderFrontendOutput = base_path('resources/views/vendor/cookieConsent/');

        $this->publishes([
            $folderFrontend . 'dialogContents.blade.stub' => $folderFrontendOutput . 'dialogContents.blade.php',
            $folderFrontend . 'index.blade.stub' => $folderFrontendOutput . 'index.blade.php',
        ], 'views-vendor');
    }

    private function publishRoutesLangFiles()
    {
        $this->publishes([
            __DIR__.'/../templates/langs/en/routes.stub' => base_path('resources/lang/en/routes.php'),
        ], 'langs');
    }

    private function publishLaravelLocalization()
    {
        $command = new ConsoleOutput();
        $command->writeln('Publishing LaravelLocalizationServiceProvider!');
        $str = 'php artisan vendor:publish --provider="Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider"';
        exec($str);
        $command->writeln('Finish Publish LaravelLocalizationServiceProvider!');
    }
}

<?php

namespace RefineriaWeb\IberostarMicrositesFrontend;

use Illuminate\Support\Facades\Facade;

class IberostarMicrositesFrontendFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'iberostar-microsites-frontend';
    }

}

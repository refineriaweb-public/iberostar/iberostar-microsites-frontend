<?php

namespace RefineriaWeb\IberostarMicrositesFrontend\Controllers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Cache;
use Illuminate\Filesystem\Filesystem;
use Spatie\Sitemap\SitemapGenerator;

class BaseController
{
    /** @var int $websiteId Website ID */
    protected static $websiteId;
    /** @var string $currentLang Current Lang. Example: 'en', 'es', 'de' */
    protected static $currentLang;

    /**
     * @return int
     */
    protected static function getWebsiteId(): int
    {
        return self::$websiteId;
    }

    /**
     * @param int $websiteId
     */
    protected static function setWebsiteId(int $websiteId)
    {
        self::$websiteId = $websiteId;
    }

    protected static function getCurrentLang(): string
    {
        return self::$currentLang;
    }

    /**
     * @param string $currentLang
     */
    protected static function setCurrentLang(string $currentLang)
    {
        self::$currentLang = $currentLang;
    }

    public function __construct()
    {
        $websiteId = Config::get('iberostar-microsites-frontend.ib_cms_website_id');
        $currentLang = \LaravelLocalization::getCurrentLocale();

        self::setWebsiteId($websiteId);
        self::setCurrentLang($currentLang);
        self::getStaticText();
        self::updateSitemap();

        $website = self::getWebsiteInfo();

        View::share([
            'websiteFolder' => Config::get('iberostar-microsites-frontend.ib_cms_api_website_name'),
            'websiteRouteName' => Config::get('iberostar-microsites-frontend.ib_cms_api_website_name'),
            'pathFile' => Config::get('iberostar-microsites-frontend.ib_cms_url_files'),
            'pathCMS' => Config::get('iberostar-microsites-frontend.ib_cms_url'),
            'website' => $website,
        ]);
    }

    /**
     * Get Static Text
     */
    public static function getStaticText()
    {
        $minutesCache = 15;
        $method = 'get-static-texts';
        $cacheName = $method . self::getCurrentLang();

        Cache::remember($cacheName, $minutesCache, function () use ($method) {
            $staticTexts = self::getInfo($method);

            foreach ($staticTexts as $fileName => $staticText) {
                self::updateStaticFile($fileName, (array)$staticText);
            }
            return true;
        });
    }

    /**
     * Get Website Info
     * @return mixed
     */
    public static function getWebsiteInfo()
    {
        $minutesCache = 15;
        $method = 'get-website-info';
        $cacheName = $method . self::getCurrentLang();

        return Cache::remember($cacheName, $minutesCache, function () use ($method) {
            return self::getInfo($method);
        });
    }

    /**
     * Get Info using the API
     * @param string $method Method
     * @param array $data Data
     * @return mixed|null
     */
    public static function getInfo(string $method, array $data = [])
    {
        if (! isset($data['lang_code'])) {
            $data['lang_code'] = self::getCurrentLang();
        }

        $dataParsed = self::parseFields($data);

        $url = sprintf(
            Config::get('iberostar-microsites-frontend.ib_cms_api_url'),
            Config::get('iberostar-microsites-frontend.ib_cms_api_website_name')
        );

        $client = new Client();
        $dataAPI = $client->request('POST', $url . $method, [
            'headers' => [
                'x-authorization' => Config::get('iberostar-microsites-frontend.ib_cms_api_key')
            ],
            'multipart' => $dataParsed
        ]);

        if ($dataAPI->getStatusCode() !== 200) {
            self::errorManager();
        }

        return json_decode($dataAPI->getBody()->getContents());
    }

    /**
     * Update Sitemap
     * @param bool $force Force Update Sitemap
     * @return bool
     */
    public static function updateSitemap(bool $force = false)
    {
        if (!Config::get('iberostar-microsites-frontend.ib_cms_sitemap_automatic')) {
            return false;
        }

        $cacheName = 'sitemap';

        if ($force) {
            Cache::forget($cacheName);
        }

        $minutesCache = 1440; // 1440 = 1día (24h x 60m)
        Cache::remember($cacheName, $minutesCache, function () {
            $sitemapPath = public_path('sitemap.xml');
            SitemapGenerator::create(env('APP_URL'))
                ->writeToFile($sitemapPath);
            return true;
        });
        return true;
    }

    /**
     * Generate Sitemap
     * @return bool
     */
    public static function generateSitemap()
    {
        return self::updateSitemap(true);
    }

    /**
     * Parse Fields
     * @param array $data
     * @return array
     */
    private static function parseFields(array $data)
    {
        $dataParsed = [];
        foreach ($data as $key => $value) {
            $dataParsed[] = [
                'name' => $key,
                'contents' => $value
            ];
        }
        return $dataParsed;
    }

    /**
     * Update Static File
     * @param string $fileName File name
     * @param array $textSepartedByLang Langs Text array
     */
    private static function updateStaticFile(string $fileName, array $textSepartedByLang)
    {
        $websiteName = env('APP_NAME', 'Laravel');

        $fileSystem = new Filesystem();

        foreach ($textSepartedByLang as $lang => $langTexts) {
            $langPath = App::langPath() . '/' . $lang . '/';
            $tab = "    ";
            $text = "<?php\n\n";
            $text .= "return [\n\n";
            $text .= "$tab/*\n";
            $text .= "$tab|--------------------------------------------------------------------------\n";
            $text .= "$tab| " . ucwords($fileName) . " [" . strtoupper($lang) . "] " . $websiteName . "\n";
            $text .= "$tab|--------------------------------------------------------------------------\n";
            $text .= "$tab*/\n\n";

            if (is_array($langTexts)) {
                ksort($langTexts);
            }

            foreach ($langTexts as $key => $value) {
                $key = str_slug($key, '-');
                $text .= "$tab'{$key}' => '".addslashes($value)."',\n";
            }

            $text .= "];\n";

            // Check if folder lang exists
            if (! $fileSystem->isDirectory($langPath)) {
                $fileSystem->makeDirectory($langPath);
            }

            $fileSystem->put($langPath . '/' . $fileName . '.php', $text);
        }
    }

    /**
     * Translate Route
     * @param string $locale Lang Local
     * @param string $transKey
     * @param string|null $attribute
     * @return string URL
     */
    public static function RWTransRoute(string $locale, string $transKey, $attribute = null)
    {
        if (is_array($transKey)) {
            $url = $locale;

            foreach ($transKey as $key) {
                $url .= "/" . trans($key, [], $locale);
            }
        } else {
            $url = $locale . "/" . trans($transKey, [], $locale);
        }

        if (!is_null($attribute)) {
            $url .= "/" . $attribute;
        }

        return $url;
    }

    public static function cacheClear()
    {
        Artisan::call('cache:clear');
        return true;
    }

    public static function errorManager()
    {
        return view('error');
    }
}

<?php

return [
    'ib_cms_website_id' => env('IB_CMS_WEBSITE_ID', ''),
    'ib_cms_api_website_name' => env('IB_CMS_API_WEBSITE_NAME', ''),
    'ib_cms_api_key' => env('IB_CMS_API_KEY', ''),
    'ib_cms_api_url' => env('IB_CMS_API_URL', 'http://iberostar-cms.rwdesarrollos.es/api/v1/websites/%s/'),
    'ib_cms_url' => env('IB_CMS_URL', 'http://iberostar-cms.rwdesarrollos.es'),
    'ib_cms_url_files' => env('IB_CMS_URL_FILES', 'http://iberostar-cms.rwdesarrollos.es/files/'),
    'ib_cms_sitemap_automatic' => env('IB_CMS_SITEMAP_AUTOMATIC', true),
];

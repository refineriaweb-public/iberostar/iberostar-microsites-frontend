# Iberostar Microsites FrontEnd
=======================

## Installation
For use with Laravel 5. This package can be installed through Composer.

``` javascript
{
  "require": {
    "refineriaweb/iberostar-microsites-frontend": "dev-master"
  },
  ...
  "repositories": [
    {
      "type": "git",
      "url": "git@gitlab.com:refineriaweb/iberostar/iberostar-microsites-frontend.git"
    }
  ]
}
```
In Laravel 5.5, the service provider and facade will automatically get registered. For older versions of the framework, follow the steps below:

Register the service provider in <code>config/app.php</code>

``` php
'providers' => [
    '...',
    RefineriaWeb\IberostarMicrositesFrontend\IberostarMicrositesFrontendServiceProvider::class,
    Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider::class,
];
```

You may also register the facade:

``` php
'aliases' => [
	'...',
	'IberostarMicrositesFrontend' => RefineriaWeb\IberostarMicrositesFrontend\IberostarMicrositesFrontendFacade::class,
	'LaravelLocalization' => Mcamara\LaravelLocalization\Facades\LaravelLocalization::class,
]
```

Publish the config file of the package using artisan

``` php
php artisan vendor:publish --provider="RefineriaWeb\IberostarMicrositesFrontend\IberostarMicrositesFrontendServiceProvider"
php artisan vendor:publish --provider="Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider"
php artisan vendor:publish --provider="Spatie\Sitemap\SitemapServiceProvider" --tag="config"
php artisan vendor:publish --provider="Spatie\CookieConsent\CookieConsentServiceProvider" --tag="config"
```

After the config file has been created locate at:

``` php
/config/iberostar-microsites-frontend.php
/config/laravellocalization.php
```

Update <code></code> indicating de supported locales and hide default locale:
``` php
return [

    // Uncomment the languages that your site supports - or add new ones.
    // These are sorted by the native name, which is the order you might show them in a language selector.
    // Regional languages are sorted by their base language, so "British English" sorts as "English, British"
    'supportedLocales' => [
        ...
        'en'          => ['name' => 'English',                'script' => 'Latn', 'native' => 'English', 'regional' => 'en_GB'],
        //'en-AU'       => ['name' => 'Australian English',     'script' => 'Latn', 'native' => 'Australian English', 'regional' => 'en_AU'],
        //'en-GB'       => ['name' => 'British English',        'script' => 'Latn', 'native' => 'British English', 'regional' => 'en_GB'],
        //'en-US'       => ['name' => 'U.S. English',           'script' => 'Latn', 'native' => 'U.S. English', 'regional' => 'en_US'],
        'es'          => ['name' => 'Spanish',                'script' => 'Latn', 'native' => 'español', 'regional' => 'es_ES'],
        ...
    ],
    ...
    'hideDefaultLocaleInURL' => true,
```


Register middleware for Laravel Localization:
```php
<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {
	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		/**** OTHER MIDDLEWARE ****/
		'localize' => \Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRoutes::class,
		'localizationRedirect' => \Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter::class,
		'localeSessionRedirect' => \Mcamara\LaravelLocalization\Middleware\LocaleSessionRedirect::class,
                'localeViewPath' => \Mcamara\LaravelLocalization\Middleware\LaravelLocalizationViewPath::class
		// REDIRECTION MIDDLEWARE
	];
}
```

Env variables
``` dotenv
IB_CMS_WEBSITE_ID= # Integer number. Example: 13
IB_CMS_API_WEBSITE_NAME= # String, lowercase, withoutspaces. Example: lasletras
IB_CMS_API_KEY=
IB_CMS_API_URL= # Default: 'http://iberostar-cms.rwdesarrollos.es/api/v1/websites/%s/'
IB_CMS_URL= # Default: 'http://iberostar-cms.rwdesarrollos.es'
IB_CMS_URL_FILES = # Default: 'http://iberostar-cms.rwdesarrollos.es/files/'
```

Update <code>wwebpack.mix.js</code>:
```javascript
let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();
}

mix.browserSync({
    proxy: 'my-domain.local'
});
```

Launch npm prod to create version css, js and manifiest files:
```
npm run prod
```
